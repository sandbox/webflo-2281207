<?php

/**
 * @file
 * Contains \Drupal\og\Access\OgMembershipAddAccessCheck.
 */

namespace Drupal\og\Access;

use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Route;

class OgMembershipAddAccessCheck implements AccessInterface {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * Constructs a EntityCreateAccessCheck object.
   *
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager.
   */
  public function __construct(EntityManagerInterface $entity_manager) {
    $this->entityManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function access(Route $route, Request $request, AccountInterface $account) {
    if ($request->attributes->has('entity_type') && $request->attributes->has('entity_id')) {
      $group = $this->loadGroup($request->attributes->get('entity_type'), $request->attributes->get('entity_id'));
      return og_ui_user_access_group('add user', $group->getEntityTypeId(), $group->id(), $account) ? static::ALLOW : static::DENY;
    }
    return static::DENY;
  }

  /**
   * Load group entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   */
  protected function loadGroup($entity_type, $entity_id) {
    if (!$this->entityManager->getDefinition($entity_type)) {
      throw new NotFoundHttpException();
    }
    $storage = $this->entityManager->getStorage($entity_type);
    return $storage->load($entity_id);
  }

}
