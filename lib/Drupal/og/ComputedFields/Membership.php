<?php

namespace Drupal\og\ComputedFields;

use Drupal\Core\TypedData\DataDefinitionInterface;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\entity_reference\Plugin\Field\FieldType\ConfigurableEntityReferenceFieldItemList;

class Membership extends ConfigurableEntityReferenceFieldItemList {

  public function __construct(DataDefinitionInterface $definition, $name = NULL, TypedDataInterface $parent = NULL) {
    parent::__construct($definition, $name, $parent);

    $states = array();

    // Get the state from settings, if exists.
    if ($state = $this->getSetting('state')) {
      $states[] = $state;
    }

    if ($gids = og_get_entity_groups($this->parent->getEntityTypeId(), $this->parent, $states)) {
      $ids = array();
      foreach ($gids as $group_type => $values) {
        $ids = array_merge($ids, array_keys($values));
      }
      $this->setValue($ids);
    }
  }

}
