<?php

/**
 * @file
 * Contains \Drupal\og\Entity\OgMembership.
 */

namespace Drupal\og\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageControllerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\FieldDefinition;
use Drupal\field\Field;

/**
 * Defines the og_membership entity.
 *
 * @ContentEntityType(
 *   id = "og_membership",
 *   label = @Translation("OG membership"),
 *   bundle_label = @Translation("OG Membership type"),
 *   module = "og",
 *   controllers = {
 *     "storage" = "\Drupal\Core\Entity\ContentEntityDatabaseStorage",
 *     "access" = "Drupal\og\OgMembershipAccessController",
 *     "form" = {
 *       "default" = "Drupal\og\OgMembershipFormController",
 *       "edit" = "Drupal\og\OgMembershipFormController",
 *       "delete" = "Drupal\og\Form\OgMembershipDeleteForm"
 *     }
 *   },
 *   admin_permission = "administer group",
 *   base_table = "og_membership",
 *   fieldable = TRUE,
 *   translatable = TRUE,
 *   render_cache = FALSE,
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "bundle" = "type"
 *   },
 *   bundle_keys = {
 *     "bundle" = "name"
 *   },
 *   bundle_entity_type = "og_membership_type",
 *   links = {
 *     "edit-form" = "og_ui.membership_edit",
 *     "delete-form" = "og_ui.membership_delete_confirm",
 *   }
 * )
 */
class OgMembership extends ContentEntityBase {

  /**
   * @return \Drupal\Core\Entity\EntityInterface
   */
  public function getGroup() {
    $storage = \Drupal::entityManager()->getStorage($this->group_type->value);
    return $storage->load($this->gid->value);
  }

  /**
   *
   * @return \Drupal\Core\Entity\EntityInterface
   */
  public function getMember() {
    $storage = \Drupal::entityManager()->getStorage($this->entity_type->value);
    return $storage->load($this->etid->value);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields['id'] = FieldDefinition::create('integer')
      ->setLabel("The group membership's unique ID.")
      ->setReadOnly(TRUE);

    $fields['uuid'] = FieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The membership UUID.'))
      ->setReadOnly(TRUE);

    $fields['type'] = FieldDefinition::create('entity_reference')
      ->setLabel(t('Type'))
      ->setDescription(t('Reference to a group membership type.'))
      ->setSetting('target_type', 'og_membership_type')
      ->setReadOnly(TRUE);

    $fields['etid'] = FieldDefinition::create('integer')
      ->setLabel("The entity ID.");

    $fields['entity_type'] = FieldDefinition::create('string')
      ->setLabel("The entity type (e.g. node, comment, etc').");

    $fields['gid'] = FieldDefinition::create('integer')
      ->setLabel("The group's unique ID.");

    $fields['group_type'] = FieldDefinition::create('string')
      ->setLabel("The group's entity type (e.g. node, comment, etc').");

    $fields['state'] = FieldDefinition::create('string')
      ->setLabel('The state of the group content.');

    $fields['created'] = FieldDefinition::create('created')
      ->setLabel('The Unix timestamp when the group content was created.');

    $fields['field_name'] = FieldDefinition::create('string')
      ->setLabel("The name of the field holding the group ID, the OG membership is associated with.");

    /*
    $fields['language'] = FieldDefinition::create('language')
      ->setLabel(t('Language code'))
      ->setDescription(t('The node language code.'));
    */

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function delete() {
    parent::delete();
    og_membership_invalidate_cache();
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);
    $uri_route_parameters['entity_type'] = $this->group_type->value;
    $uri_route_parameters['entity_id'] = $this->gid->value;
    return $uri_route_parameters;
  }



}
