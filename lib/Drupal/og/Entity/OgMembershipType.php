<?php

/**
 * @file
 * Contains \Drupal\og\Entity\OgMembershipType.
 */

namespace Drupal\og\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the og_membership_type entity class.
 *
 * @ConfigEntityType(
 *   id = "og_membership_type",
 *   label = @Translation("OG Membership Type"),
 *   config_prefix = "membership_type",
 *   bundle_of = "og_membership",
 *   entity_keys = {
 *     "id" = "name",
 *     "uuid" = "uuid"
 *   }
 * )
 */
class OgMembershipType extends ConfigEntityBase {

  /**
   * The ID.
   *
   * @var string
   */
  public $name;

  /**
   * The UUID.
   *
   * @var string
   */
  public $uuid;

  /**
   * The description.
   *
   * @var string
   */
  public $description;

}
