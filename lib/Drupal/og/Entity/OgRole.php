<?php

/**
 * @file
 * Contains \Drupal\og\Entity\OgRole.
 */

namespace Drupal\og\Entity;
use Drupal\user\Entity\Role;

/**
 * Defines the user role entity class.
 *
 * @ConfigEntityType(
 *   id = "og_user_role",
 *   label = @Translation("OG Role"),
 *   admin_permission = "administer permissions",
 *   config_prefix = "role",
 *   entity_keys = {
 *     "id" = "id",
 *     "weight" = "weight",
 *     "label" = "label"
 *   },
 *   controllers = {
 *     "storage" = "Drupal\og\OgRoleStorageController"
 *   }
 * )
 */
class OgRole extends Role {

  public $name = NULL;

  public $group_type = NULL;

  public $group_bundle = NULL;

}
