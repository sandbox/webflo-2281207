<?php

/**
 * @file
 * Contains \Drupal\og\Form\OgMembershipDeleteForm.
 */

namespace Drupal\og\Form;

use Drupal\Core\Entity\ContentEntityConfirmFormBase;

class OgMembershipDeleteForm extends ContentEntityConfirmFormBase {

  protected $group;

  protected $member;

  /**
   * Returns the question to ask the user.
   *
   * @return string
   *   The form question. The page title will be set to this value.
   */
  public function getQuestion() {
    return $this->t('Are you sure you would like to remove the membership for the user @user?', array('@user' => $this->member->label()));
  }

  /**
   * Returns the route to go to if the user cancels the action.
   *
   * @return array
   *   An associative array with the following keys:
   *   - route_name: The name of the route.
   *   - route_parameters: (optional) An associative array of parameter names
   *     and values.
   *   - options: (optional) An associative array of additional options. See
   *     \Drupal\Core\Routing\UrlGeneratorInterface::generateFromRoute() for
   *     comprehensive documentation.
   */
  public function getCancelRoute() {
    return $this->group->urlInfo();
  }

  public function buildForm(array $form, array &$form_state) {
    $this->group = $this->getEntity()->getGroup();
    $this->member = $this->getEntity()->getMember();

    if ($this->member->getEntityTypeId() == 'user' && $this->member->id() == $this->group->uid->value) {
      return array(
        '#markup' => $this->t("You can't remove membership of the group manager")
      );
    }

    return parent::buildForm($form, $form_state);
  }

}
