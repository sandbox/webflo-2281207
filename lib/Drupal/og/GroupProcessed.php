<?php

/**
 * @file
 * Contains \Drupal\og\GroupProcessed.
 */

namespace Drupal\og;

use Drupal\Core\TypedData\TypedData;

class GroupProcessed extends TypedData {

  protected $membership = NULL;

  public function getValue($langcode = NULL) {
    if ($this->membership !== NULL) {
      return $this->membership;
    }

    $entity = $this->parent->getEntity();
    $field = $this->parent->getFieldDefinition();
    $this->membership = og_get_membership($field->getSetting('target_type'), $this->parent->target_id, $entity->getEntityTypeId(), $entity->id());
    return $this->membership;
  }

} 
