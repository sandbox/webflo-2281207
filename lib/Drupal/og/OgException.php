<?php

/**
 * @file
 * Provide a separate Exception so it can be caught separately.
 */

namespace Drupal\og;

use Exception;

class OgException extends Exception {}
