<?php

/**
 * @file
 * Contains \Drupal\og\OgMembershipAccessController.
 */

namespace Drupal\og;

use Drupal\Core\Entity\EntityAccessController;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides access control for og membership entity.
 */
class OgMembershipAccessController extends EntityAccessController {

  protected function checkAccess(EntityInterface $entity, $operation, $langcode, AccountInterface $account) {
    if ($operation == 'delete') {
      $member = $entity->getMember();
      $group = $entity->getGroup();
      if ($member->getEntityTypeId() == 'user' && $member->id() == $group->uid->value) {
        return FALSE;
      }
    }

    $access = parent::checkAccess($entity, $operation, $langcode, $account);
    if ($access) {
      return $account;
    }
    return og_ui_user_access_group('manage members', $entity->group_type->value, $entity->gid->value);
  }

  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    $access = parent::checkCreateAccess($account, $context, $entity_bundle);
    if ($access) {
      return $account;
    }
    // return og_ui_user_access_group('add user', $entity->group_type->value, $entity->gid->value);
  }

}
