<?php

/**
 * @file
 * Contains \Drupal\og\OgMembershipFormController.
 */

namespace Drupal\og;

use Drupal\Core\Entity\ContentEntityFormController;

class OgMembershipFormController extends ContentEntityFormController {

  public function form(array $form, array &$form_state) {
    $form = parent::form($form, $form_state);

    $group_type = $this->entity->group_type->value;
    $gid = $this->entity->gid->value;
    $group = entity_load($group_type, $gid);
    $account = user_load($this->entity->etid->value);

    // Get all the non-default roles.
    $og_roles = og_roles($group_type, $this->entity->bundle(), $gid, FALSE, FALSE);

    $form['group_type'] = array('#type' => 'value', '#value' => $group_type);
    $form['gid'] = array('#type' => 'value', '#value' => $gid);
    $form['id'] = array('#type' => 'value', '#value' => $this->entity->id());

    $form['og_user'] = array(
      '#type' => 'fieldset',
      '#title' => t('Edit a group membership in %group', array('%group' => $group->label())),
    );
    $form['og_user']['name'] = array(
      '#type' => 'markup',
      '#title' => t('User name'),
      '#markup' => ($account) ? $account->getUsername() : NULL,
    );
    $form['og_user']['state'] = array(
      '#type' => 'select',
      '#title' => t('Status'),
      '#description' => t('Change the current membership status.'),
      '#options' => og_group_content_states(),
      '#default_value' => $this->entity->state->value,
    );
    if ($og_roles) {
      $form['og_user']['roles'] = array(
        '#type' => 'checkboxes',
        '#options' => $og_roles,
        '#title' => t('Roles'),
        '#default_value' => array_keys(og_get_user_roles($group_type, $gid, $account->uid)),
      );
    }

    $form['membership_fields'] = array(
      '#prefix' => '<div id="og-ui-field-name">',
      '#suffix' => '</div>',
      '#tree' => TRUE,
      '#parents' => array('membership_fields'),
    );

    return $form;
  }

  public function submit(array $form, array &$form_state) {
    $og_membership = $this->entity;

    $group_type = $form_state['values']['group_type'];
    $gid = $form_state['values']['gid'];
    $og_membership->state = $form_state['values']['state'];

    $account = user_load($og_membership->etid->value);
    // Assign roles.
    $og_roles = og_get_user_roles($group_type, $gid, $account->id());
    foreach (array_keys($og_roles) as $rid) {
      if (!in_array($rid, $form_state['values']['roles'])) {
        og_role_revoke($group_type, $gid, $account->id(), $rid);
      }
    }
    if (!empty($form_state['values']['roles'])) {
      foreach ($form_state['values']['roles'] as $rid) {
        og_role_grant($group_type, $gid, $og_membership->etid->value, $rid);
      }
    }

    // Saving should be last in the process, so Rules can get a chance to
    // assign or revoke roles.
    $og_membership->save();

    drupal_set_message(t('The membership has been updated.'));
  }

}
