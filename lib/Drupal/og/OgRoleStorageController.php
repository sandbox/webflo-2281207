<?php

/**
 * @file
 * Contains \Drupal\og\OgRoleStorageController.
 */

namespace Drupal\og;

use Drupal\user\RoleStorage;

/**
 * Controller class for og user roles.
 */
class OgRoleStorageController extends RoleStorage {

  /**
   * {@inheritdoc}
   */
  public function deleteRoleReferences(array $rids) {
    db_delete('og_role')
      ->condition('rid', $rids)
      ->execute();
    db_delete('og_role_permission')
      ->condition('rid', $rids)
      ->execute();
    // Update the users who have this role set.
    db_delete('og_users_roles')
      ->condition('rid', $rids)
      ->execute();
  }

}
