<?php

/**
 * @file
 * Contains \Drupal\og\Plugin\Field\FieldType\OgMembershipReferenceFieldItemList.
 */

namespace Drupal\og\Plugin\Field\FieldType;

use Drupal\Core\TypedData\DataDefinitionInterface;
use Drupal\Core\TypedData\TypedDataInterface;
use Drupal\entity_reference\Plugin\Field\FieldType\ConfigurableEntityReferenceFieldItemList;

class OgMembershipReferenceFieldItemList extends ConfigurableEntityReferenceFieldItemList {

  /**
   * {@inheritdoc}
   */
  public function __construct(DataDefinitionInterface $definition, $name = NULL, TypedDataInterface $parent = NULL) {
    parent::__construct($definition, $name, $parent);

    // Load group memberships
    if ($this->getEntity()->id()) {
      $target_type = $this->getSetting('target_type');
      $items = og_get_entity_groups($this->getEntity()->getEntityTypeId(), $this->getEntity()->id(), array(), $this->getFieldDefinition()->getName());
      if (isset($items[$target_type]))  {
        foreach (array_values($items[$target_type]) as $delta => $target_id) {
          $this->list[$delta] = $this->createItem($delta, array('target_id' => $target_id));
        }
      }
    }
  }

  public function insert() {
    parent::update();
    $this->OgMembershipCrud();
    $this->list = array();
  }

  public function update() {
    parent::update();
    $this->OgMembershipCrud();
    $this->list = array();
  }

  public function delete() {
    parent::delete();
    foreach ($this->list as $item) {
      if ($item->gid && $item->gid->id()) {
        $item->gid->delete();
      }
    }
    $this->list = array();
  }

  protected function OgMembershipCrud() {
    $items = $this;
    $field_name = $this->getName();
    $entity_id = $this->getEntity()->id();
    $entity_type = $this->getEntity()->getEntityTypeId();

    if (!$diff = $this->groupAudiencegetDiff($items)) {
      return;
    }

    $group_type = $this->getSetting('target_type');
    $diff += array('insert' => array(), 'delete' => array());

    // Delete first, so we don't trigger cardinality errors.
    if ($diff['delete']) {
      $membership_ids = array();
      foreach ($diff['delete'] as $item) {
        if (isset($item['membership'])) {
          $membership_ids[] = $item['membership'];
        }
      }
      entity_delete_multiple('og_membership', $membership_ids);
    }

    if (!$diff['insert']) {
      return;
    }

    // Prepare an array with the membership state, if it was provided in the widget.
    $states = array();
    foreach ($items as $item) {
      $gid = $item->target_id;
      if (empty($item->state) || !$diff['insert'][$gid]) {
        // State isn't provided, or not an "insert" operation.
        continue;
      }
      $states[$gid] = $item->state;
    }

    foreach ($diff['insert'] as $gid => $item) {
      $values = array(
        'entity_type' => $entity_type,
        'entity' => $entity_id,
        'field_name' => $field_name,
      );

      if (!empty($states[$gid])) {
        $values['state'] = $states[$gid];
      }

      $group = entity_load($group_type, $gid);
      og_group($group, $values, TRUE);
    }
  }

  protected function groupAudiencegetDiff($items) {
    $return = FALSE;

    $unchanged_entity = \Drupal::entityManager()
      ->getStorage($items->getParent()->getEntityTypeId())
      ->loadUnchanged($items->getParent()->id());

    $og_memberships = $unchanged_entity->{$this->getName()};

    $insert = array();
    foreach ($items as $item) {
      $insert[$item->target_id]['target_id'] = $item->target_id;
    }

    $delete = array();
    foreach ($og_memberships as $og_membership) {
      if (!isset($insert[$og_membership->target_id])) {
        $membership_entity = $og_membership->membership;
        $is_group_owner = $this->isGroupOwner($og_membership);
        if ($is_group_owner) {
          $add[$og_membership->target_id]['target_id'] = $og_membership->target_id;
        } else {
          $delete[$og_membership->target_id] = array(
            'target_id' => $og_membership->target_id,
            'membership' => $membership_entity ? $membership_entity->id() : NULL,
          );
        }
      }
    }

    $return['insert'] = $insert;
    $return['delete'] = $delete;
    return $return;
  }

  protected function isGroupOwner($item) {
    $group = $item->entity;
    if ($group && $this->getEntity()->getEntityTypeId() == 'user' && $this->getEntity()->id() == $group->uid->value) {
      return TRUE;
    }
    return FALSE;
  }

}
