<?php

/**
 * @file
 * Contains \Drupal\og\Plugin\Field\FieldType\OgMembershipReferenceItem.
 */

namespace Drupal\og\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\entity_reference\ConfigurableEntityReferenceItem;

/**
 * Plugin implementation of the 'og_membership' field type.
 *
 * @FieldType(
 *   id = "og_membership",
 *   label = @Translation("OG Membership"),
 *   settings = {
 *     "target_type" = "node",
 *   },
 *   instance_settings = {
 *     "handler" = "default",
 *     "handler_settings" = {}
 *   },
 *   default_widget = "entity_reference_autocomplete",
 *   default_formatter = "entity_reference_label",
 *   list_class = "\Drupal\og\Plugin\Field\FieldType\OgMembershipReferenceFieldItemList"
 * )
 */
class OgMembershipReferenceItem extends ConfigurableEntityReferenceItem {

  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    // Call the parent to define the target_id and entity properties.
    $properties = parent::propertyDefinitions($field_definition);

    $properties['gid'] = DataDefinition::create('entity_reference')
      ->setReadOnly(TRUE)
      ->setComputed(TRUE)
      ->setClass('\Drupal\og\GroupProcessed');

    $properties['membership'] = DataDefinition::create('entity_reference')
      ->setReadOnly(TRUE)
      ->setComputed(TRUE)
      ->setClass('\Drupal\og\OgMembershipProcessed');

    return $properties;
  }

}
