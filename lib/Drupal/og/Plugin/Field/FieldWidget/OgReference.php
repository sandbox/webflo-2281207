<?php

/**
 * @file
 * Contains \Drupal\Drupal\og\Field\FieldWidget\OgReference.
 */

namespace Drupal\og\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\options\Plugin\Field\FieldWidget\SelectWidget;

/**
 * Plugin implementation of the 'og_complex' widget.
 *
 * @FieldWidget(
 *   id = "og_complex",
 *   label = @Translation("OG reference"),
 *   field_types = {
 *     "og_membership"
 *   },
 *   multiple_values = TRUE
 * )
 */
class OgReference extends SelectWidget {

  protected function getSelectedOptions(FieldItemListInterface $items, $delta = 0) {
    $selected = parent::getSelectedOptions($items, $delta);

    /**
     * @todo: Replace with a propper version of entity reference repopulate.
     */
    if (empty($selected) && $items->getEntity()->isNew()) {
      $options = \Drupal::request()->get('gid');
      return $options;
    }

    return $selected;
  }

  protected function getOptions(FieldItemInterface $item) {
    if (!isset($this->options)) {
      $grouped_options = array();

      // Limit the settable options for the current user account.
      $options = $item->getSettableOptions(\Drupal::currentUser());

      // Add an empty option if the widget needs one.
      if ($empty_option = $this->getEmptyOption()) {
        $label = ($empty_option == static::OPTIONS_EMPTY_NONE ? t('- None -') : t('- Select a value -'));
        $options = array('_none' => $label) + $options;
      }

      $module_handler = \Drupal::moduleHandler();
      $context = array(
        'fieldDefinition' => $this->fieldDefinition,
        'entity' => $item->getEntity(),
      );
      $module_handler->alter('options_list', $options, $context);

      array_walk_recursive($options, array($this, 'sanitizeLabel'));

      if (isset($options['_none'])) {
        $grouped_options['_none'] = $options['_none'];
        unset($options['_none']);
      }

      $target_type = $this->fieldDefinition->getSetting('target_type');
      $user_gids = og_get_entity_groups();
      $user_gids = !empty($user_gids[$target_type]) ? $user_gids[$target_type] : array();

      $entity_gids = array_keys($options);

      // Get the "Other group" group IDs.
      $other_groups_ids = array_diff($entity_gids, $user_gids);

      $your_groups_options = $this->arrayFilterByKey($user_gids, $options);
      if ($your_groups_options) {
        $your_groups = t('Your groups');
        $grouped_options[$your_groups] = $your_groups_options;
      }

      $other_group_options = $this->arrayFilterByKey($other_groups_ids, $options);
      if ($other_group_options) {
        $other_groups = t('Other groups');
        $grouped_options[$other_groups] = $other_group_options;
      }

      $this->options = $grouped_options;
    }

    return $this->options;
  }


  /*
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, array &$form_state) {
    // The group IDs that might not be accessible by the user, but we need
    // to keep even after saving.
    $element['#other_groups_ids'] = array();
    $element['#element_validate'][] = array(__CLASS__, 'elementValidate');

    if (user_access('administer group')) {
      $has_admin = TRUE;
      $field_modes[] = 'admin';
    }

    // Build an array of entity IDs. Field's $items are loaded
    // in OgBehaviorHandler::load().
    $entity_gids = array();
    foreach ($items as $item) {
      $entity_gids[] = $item->target_id;
    }

    if ($other_groups_ids) {
      foreach ($other_groups_ids as $id) {
        $element['#other_groups_ids'][] = array(
          'target_id' => $id,
          'field_mode' => 'admin',
        );
      }
    }
  }

  public function elementValidate() {

  }
  */

  protected function arrayFilterByKey($keys, $values) {
    return array_intersect_key($values, array_flip($keys));
  }

}
