<?php

/**
 * @file
 * Contains \Drupal\og\Plugin\entity_reference\selection\OgGroupSelection.
 */

namespace Drupal\og\Plugin\entity_reference\selection;

use Drupal\entity_reference\Plugin\entity_reference\selection\SelectionBase;

/**
 * Plugin implementation of the 'selection' entity_reference.
 *
 * @EntityReferenceSelection(
 *   id = "og",
 *   label = @Translation("OG"),
 *   group = "og",
 *   weight = 0,
 *   derivative = "Drupal\entity_reference\Plugin\Derivative\SelectionBase"
 * )
 */
class OgGroupSelection extends SelectionBase {

  public function buildEntityQuery($match = NULL, $match_operator = 'CONTAINS') {
    $query = parent::buildEntityQuery($match, $match_operator);

    $target_type = $this->fieldDefinition->getSetting('target_type');
    $target_type_info = \Drupal::entityManager()->getDefinition($target_type);

    if (!user_access('administer groups')) {
      $user_groups = og_get_groups_by_user(NULL, $target_type);
      $user_groups = $user_groups ? $user_groups : array();
      $user_groups = array_merge($user_groups, $this->getGidsForCreate());
      $query->condition($target_type_info->getKey('id'), $user_groups, 'IN');
    }

    return $query;
  }

  /**
   * Get group IDs from URL or OG-context, with access to create group-content.
   *
   * @return
   *   Array with group IDs a user (member or non-member) is allowed to
   * create, or empty array.
   */
  private function getGidsForCreate() {
    /*
    if ($this->instance['entity_type'] != 'node') {
      return array();
    }

    if (!empty($this->entity->nid)) {
      // Existing node.
      return array();
    }

    if (!module_exists('entityreference_prepopulate') || empty($this->instance['settings']['behaviors']['prepopulate'])) {
      return array();
    }

    // Don't try to validate the IDs.
    if (!$ids = entityreference_prepopulate_get_values($this->field, $this->instance, FALSE)) {
      return array();
    }
    $node_type = $this->instance['bundle'];
    foreach ($ids as $delta => $id) {
      if (!is_numeric($id) || !$id || !og_user_access($this->field['settings']['target_type'], $id, "create $node_type content")) {
        unset($ids[$delta]);
      }
    }
    return $ids;
    */

    return array();
  }

}
