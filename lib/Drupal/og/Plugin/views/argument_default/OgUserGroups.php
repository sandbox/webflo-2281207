<?php

/**
 * @file
 * Contains \Drupal\og\Plugin\views\OgUserGroups.
 */

namespace Drupal\og\Plugin\views\argument_default;

use Drupal\views\Plugin\views\argument_default\ArgumentDefaultPluginBase;

/**
 * The fixed argument default handler.
 *
 * @ingroup views_argument_default_plugins
 *
 * @ViewsArgumentDefault(
 *   id = "og_user_groups",
 *   title = @Translation("The OG groups of the logged in user")
 * )
 */
class OgUserGroups extends ArgumentDefaultPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['group_type'] = array(
      'default' => '',
      'bool' => FALSE,
      'translatable' => FALSE
    );
    $options['glue'] = array(
      'default' => '+',
      'bool' => FALSE,
      'translatable' => FALSE
    );
    return $options;
  }

  public function buildOptionsForm(&$form, &$form_state) {
    $form['group_type'] = array(
      '#type' => 'select',
      '#title' => t('Group type'),
      '#description' => t('Select the group type.'),
      '#options' => og_get_all_group_entity(),
      '#default_value' => $this->options['group_type'],
      '#required' => og_get_all_group_entity(),
    );

    $form['glue'] = array(
      '#type' => 'select',
      '#title' => t('Concatenator'),
      '#description' => t('Select the concatenator used to merge multiple group IDs. Remember to turn on the "Allow multiple values" option in the "more" settings for this contextual filter.'),
      '#options' => array('+' => '+', ',' => ','),
      '#default_value' => $this->options['glue'],
    );

    return $form;
  }

  public function getArgument() {
    // Get the group IDs relevant for the acting user, and return them
    // concatenated.
    if ($gids = og_get_groups_by_user(NULL, $this->options['group_type'])) {
      return implode($this->options['glue'], $gids);
    }
    return FALSE;
  }

}
