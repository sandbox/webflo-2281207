<?php

/**
 * Implements hook_views_data().
 */
function og_views_data() {

  $data['og_membership']['table']['group'] = 'OG Membership';

  $data['og_membership']['table']['base'] = array(
    'field' => 'id',
    'title' => 'OG Membership',
  );

  $data['og_membership']['table']['entity type'] = 'og_membership';

  $data['og_membership']['id'] = array(
    'title' => t('Membership id'),
    'help' => t('Og membership "id" property.'),
    'field' => array(
      'id' => 'numeric',
    ),
    'argument' => array(
      'id' => 'numeric',
    ),
    'filter' => array(
      'id' => 'numeric',
    ),
    'sort' => array(
      'id' => 'standard',
    ),
  );

  $data['og_membership']['etid'] = array(
    'title' => t('Entity id'),
    'help' => t('Og membership "etid" property.'),
    'field' => array(
      'id' => 'numeric',
    ),
    'argument' => array(
      'id' => 'numeric',
    ),
    'filter' => array(
      'id' => 'numeric',
    ),
    'sort' => array(
      'id' => 'standard',
    ),
  );

  $data['og_membership']['entity_type'] = array(
    'title' => t('Entity type'),
    'help' => t('Og membership "entity_type" property.'),
    'field' => array(
      'id' => 'standard',
    ),
    'argument' => array(
      'id' => 'standard',
    ),
    'filter' => array(
      'id' => 'equality',
    ),
    'sort' => array(
      'id' => 'standard',
    ),
  );

  $data['og_membership']['gid'] = array(
    'title' => t('Group id'),
    'help' => t('Og membership "gid" property.'),
    'field' => array(
      'id' => 'numeric',
    ),
    'argument' => array(
      'id' => 'numeric',
    ),
    'filter' => array(
      'id' => 'numeric',
    ),
    'sort' => array(
      'id' => 'standard',
    ),
  );

  $data['og_membership']['group_type'] = array(
    'title' => t('Group type'),
    'help' => t('Og membership "group_type" property.'),
    'field' => array(
      'id' => 'standard',
    ),
    'argument' => array(
      'id' => 'standard',
    ),
    'filter' => array(
      'id' => 'equality',
    ),
    'sort' => array(
      'id' => 'standard',
    ),
  );

  $data['og_membership']['created'] = array(
    'title' => t('Create date'),
    'help' => t('The date the membership was created.'),
    'field' => array(
      'id' => 'date',
    ),
    'sort' => array(
      'id' => 'date'
    ),
    'filter' => array(
      'id' => 'date',
    ),
  );

  $data['og_membership']['view_og_membership'] = array(
    'field' => array(
      'title' => t('Link to membership'),
      'id' => 'entity_link',
      'field' => 'gid',
    ),
  );

  $data['og_membership']['edit_og_membership'] = array(
    'field' => array(
      'title' => t('Link to edit membership'),
      'id' => 'entity_link_edit',
      'field' => 'id',
    ),
  );

  $data['og_membership']['delete_og_membership'] = array(
    'field' => array(
      'title' => t('Link to delete membership'),
      'id' => 'entity_link_delete',
      'field' => 'id',
    ),
  );

  return $data;
}


/**
 * Implements hook_views_data_alter().
 */
function og_views_data_alter(&$data) {
  $group_content_entities = og_get_all_group_content_entity();
  $group_entity_types = og_get_all_group_entity();

  foreach (Drupal::entityManager()->getDefinitions() as $entity_type => $info) {
    if (empty($group_content_entities[$entity_type]) && empty($group_entity_types[$entity_type])) {
      continue;
    }
    $base_table = $info->getBaseTable();

    $data[$base_table]['og_membership_rel'] = array(
      'group' => t('OG membership'),
      'title' => t('OG membership from @entity', array('@entity' => $info->getLabel())),
      'help' => t('The OG membership associated with the @entity entity.', array('@entity' => $info->getLabel())),
      'relationship' => array(
        'title' => t('OG membership from @entity', array('@entity' => $entity_type)),
        'id' => 'standard',
        'base' => 'og_membership',
        'base field' => 'etid',
        'field' => $info->getKey('id'),
        'extra' => array(
          array(
            'field' => 'entity_type',
            'value' => $entity_type,
          ),
        ),
      ),
    );

    // The OG membership group.
    $data[$base_table]['og_membership_rel_group'] = array(
      'group' => t('OG membership'),
      'title' => t('OG membership from @entity group', array('@entity' => $info->getLabel())),
      'help' => t('The OG membership associated with the @entity group', array('@entity' => $info->getLabel())),
      'relationship' => array(
        'title' => t('OG membership from @entity group', array('@entity' => $entity_type)),
        'id' => 'standard',
        'base' => 'og_membership',
        'base field' => 'gid',
        'field' => $info->getKey('id'),
        'extra' => array(
          array(
            'field' => 'group_type',
            'value' => $entity_type,
          ),
        ),
      ),
    );

    // OG membership's related entity.
    $data['og_membership']['og_membership_related_' . $entity_type] = array(
      'group' => t('OG membership'),
      'title' => t('@entity from OG membership', array('@entity' => $info->getLabel())),
      'help' => t('The @entity entity that is associated with the OG membership.', array('@entity' => $info->getLabel())),
      'relationship' => array(
        'title' => t('@entity from OG membership', array('@entity' => $entity_type)),
        'id' => 'standard',
        'base' => $base_table,
        'base field' => $info->getKey('id'),
        'field' => 'etid',
        'extra' => array(
          array(
            'field' => 'entity_type',
            'value' => $entity_type,
          ),
        ),
      ),
    );

    // OG membership's related group.
    $data['og_membership']['og_membership_related_' . $entity_type . '_group'] = array(
      'group' => t('OG membership'),
      'title' => t('Group @entity from OG membership', array('@entity' => $info->getLabel())),
      'help' => t('The @entity group that is associated with the OG membership.', array('@entity' => $info->getLabel())),
      'relationship' => array(
        'id' => 'standard',
        'title' => t('Group @entity from OG membership', array('@entity' => $entity_type)),
        'base' => $base_table,
        'base field' => $info->getKey('id'),
        'field' => 'gid',
        /*
        'extra' => array(
          array(
            'field' => 'group_type',
            'value' => $entity_type,
          ),
        ),
        */
      ),
    );
  }

  /*
  $data['field_data_group_audience']['user_roles'] = array(
    'group' => t('Group'),
    'title' => t('Group user roles'),
    'help' => t('Show all the roles a user belongs to in a group.'),
    // This is a dummy field, so point it to a real field that we need - the
    // group ID
    'real field' => 'group_audience_gid',
    'field' => array(
      'handler' => 'og_handler_field_user_roles',
    ),
  );

  $data['field_data_group_audience']['og_permissions'] = array(
    'group' => t('Group'),
    'title' => t('Group permissions'),
    'help' => t('Filter by group permissions.'),
    // This is a dummy field, so point it to a real field that we need - the
    // group ID
    'real field' => 'group_audience_gid',
    'field' => array(
      'handler' => 'og_handler_field_group_permissions',
    ),
  );
  */
}
