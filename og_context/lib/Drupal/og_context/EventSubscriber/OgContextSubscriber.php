<?php

/**
 * @file
 * Contains \Drupal\og_context\EventSubscriber\OgContextSubscriber.
 */

namespace Drupal\og_context\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class OgContextSubscriber implements EventSubscriberInterface {

  public function initOgContext(GetResponseEvent $event) {
    og_context_determine_context_info();
  }

  /**
  * {@inheritdoc}
  */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = array('initOgContext');
    return $events;
  }

}
