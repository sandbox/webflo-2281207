<?php

/**
 * @file
 * Contains \Drupal\og_context\ContextHandler\OgContextHandlerNode.
 */

namespace Drupal\og_context\Plugin\OgContextHandler;

use Symfony\Cmf\Component\Routing\RouteObjectInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Context handler; Get groups from existing node or ctools context.
 *
 * @param $node
 *  Optional; A node. If empty a node object will attempted to be loaded via
 *  menu_get_object().
 *
 * @Plugin(
 *   id = "node",
 *   name = @Translation("Node")
 * )
 */
class OgContextHandlerNode {

  protected $request;

  public function applies(Request $request) {
    $this->request = $request;

    $route_name = $this->request->attributes->get(RouteObjectInterface::ROUTE_NAME);
    switch ($route_name) {
      case 'node.view':
      case 'node.page_edit':
        return TRUE;
        break;
    }
    return FALSE;
  }

  public function getGroups() {
    /**
     * @var $entity \Drupal\Core\Entity\EntityInterface;
     */
    $entity = $this->request->attributes->get('node');
    $contexts = array();

    if ($entity) {
      if ($group = og_is_group($entity)) {
        $contexts[$entity->getEntityTypeId()][] = $entity->id();
      }
      elseif ($gids = og_get_entity_groups($entity->getEntityTypeId(), $entity)) {
        $contexts = $gids;
      }
    }

    return $contexts;
  }

}
