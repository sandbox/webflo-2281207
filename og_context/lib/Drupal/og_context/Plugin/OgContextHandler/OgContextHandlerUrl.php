<?php

namespace Drupal\og_context\Plugin\OgContextHandler;

use Symfony\Component\HttpFoundation\Request;

/**
 * Context handler; Get groups from node create URL.
 *
 * @Plugin(
 *   id = "url",
 *   name = @Translation("URL")
 * )
 */
class OgContextHandlerUrl {

  protected $request;

  public function applies(Request $request) {

  }

  public function getGroups() {
    if (!module_exists('entityreference_prepopulate')) {
      return;
    }
    $item = menu_get_item();
    if (strpos($item['path'], 'node/add/') !== 0) {
      return;
    }
    if (empty($item['map'][2])) {
      // If we don't have this key in the array, it means the user doesn't have
      // access to create this node.
      return;
    }
    $node_type = str_replace('-', '_', $item['map'][2]);

    if (!$fields = og_get_group_audience_fields('node', $node_type)) {
      return;
    }
    $gids = array();
    foreach ($fields as $field_name => $label) {
      $field = field_info_field($field_name);
      $instance = field_info_instance('node', $field_name, $node_type);
      if ($ids = entityreference_prepopulate_get_values_from_url($field, $instance)) {
        // We need to validate those values ourself, as we called
        // entityreference_prepopulate_get_values_from_url() directly, bypassing
        // entityreference_prepopulate_get_values().
        $target_type = $field['settings']['target_type'];
        $valid_ids = array();
        $entities = entity_load($target_type, $ids);
        foreach ($entities as $id => $entity) {
          if (!entity_access('view', $target_type, $entity)) {
            // User can't access entity.
            continue;
          }

          if (!og_is_group($target_type, $entity)) {
            // Entity is not a group.
            continue;
          }

          $valid_ids[] = $id;
        }

        if ($valid_ids) {
          $gids += array($target_type => array());
          $gids[$target_type] = array_merge($gids[$target_type], $valid_ids);
        }
      }
    }
    return $gids;
  }

}
