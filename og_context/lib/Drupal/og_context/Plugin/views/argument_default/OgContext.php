<?php

/**
 * @file
 * Contains \Drupal\og_context\Plugin\views\argument_default\OgContext.
 */

namespace Drupal\og_context\Plugin\views\argument_default;

use Drupal\views\Plugin\views\argument_default\ArgumentDefaultPluginBase;

/**
 * The group context argument default plugin.
 *
 * @ingroup views_argument_default_plugins
 *
 * @ViewsArgumentDefault(
 *   id = "og_context",
 *   title = @Translation("Current OG group from context")
 * )
 */
class OgContext extends ArgumentDefaultPluginBase {

  protected function defineOptions() {
    $options['group_type'] = array('default' => 'node');
    return $options;
  }

  public function buildOptionsForm(&$form, &$form_state) {
    $form['group_type'] = array(
      '#type' => 'select',
      '#options' => og_get_all_group_entity(),
      '#title' => t('Group type'),
      '#default_value' => $this->options['group_type'],
      '#description' => t('Determine what entity type that group should be of.')
    );
  }

  public function getArgument() {
    if ($group = og_context($this->options['group_type'])) {
      return $group['gid'];
    }
    return FALSE;
  }

}
