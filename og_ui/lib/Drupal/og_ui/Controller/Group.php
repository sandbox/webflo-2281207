<?php

/**
 * @file
 * Contains \Drupal\og_ui\Controller\Group.
 */

namespace Drupal\og_ui\Controller;

use Drupal\Core\Field\FieldDefinition;
use Drupal\field\Field;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Controller for common group operations.
 */
class Group extends OgControllerBase {

  public function subscribe($entity_type, $entity_id) {
    $this->loadGroup($entity_type, $entity_id);
    $bundle = $this->group->bundle();

    if (empty($field_name)) {
      $field_name = og_get_best_group_audience_field('user', $this->account, $entity_type, $bundle);
      if (empty($field_name)) {
        // User entity has no group audience field.
        throw new NotFoundHttpException();
      }
    }

    $field = Field::fieldInfo()->getInstance($this->account->getEntityTypeId(), $this->account->bundle(), $field_name);
    if (!$field) {
      // Field name given is incorrect
      throw new NotFoundHttpException();
    }

    $field_access = $this->entityManager()->getAccessController($this->account->getEntityTypeId())->fieldAccess('view', $field);
    if (!$field_access) {
      // User doesn't have access to the field.
      throw new NotFoundHttpException();
    }

    if (!$this->account->id()) {
      // Anonymous user can't request membership.
      $dest = drupal_get_destination();
      if ($this->config('og.settings')->get('user_register')) {
        drupal_set_message(t('In order to join any group, you must <a href="!login">login</a>. After you have successfully done so, you will need to request membership again.', array('!login' => url("user/login", array('query' => $dest)))));
      }
      else {
        drupal_set_message(t('In order to join any group, you must <a href="!login">login</a> or <a href="!register">register</a> a new account. After you have successfully done so, you will need to request membership again.', array(
          '!register' => url("user/register", array('query' => $dest)),
          '!login' => url("user/login", array('query' => $dest))
        )));
      }
      return $this->redirect('user.login');
    }

    $redirect = FALSE;
    $message = '';

    $params = array();
    $params['@user'] = \Drupal::currentUser()->getUsername();

    // Show the group name only if user has access to it.
    $params['@group'] = $this->group->access('view') ? $this->group->label() : t('Private group');

    if (og_is_member($this->group, 'user', $this->account, array(OG_STATE_BLOCKED))) {
      // User is blocked, access denied.
      throw new AccessDeniedHttpException();
    }
    if (og_is_member($this->group, 'user', $this->account, array(OG_STATE_PENDING))) {
      // User is pending, return them back.
      $message = $this->account->id() == $this->account->id() ? t('You already have a pending membership for the group @group.', $params) : t('@user already has a pending membership for the  the group @group.', $params);
      $redirect = TRUE;
    }

    if (og_is_member($this->group, 'user', $this->account, array(OG_STATE_ACTIVE))) {
      // User is already a member, return them back.
      $message = $this->account->id() == $this->account->id() ? t('You are already a member of the group @group.', $params) : t('@user is already a member of the group @group.', $params);
      $redirect = TRUE;
    }

    if (!$message && $field->getCardinality() != FieldDefinition::CARDINALITY_UNLIMITED) {
      // Check if user is already registered as active or pending in the maximum
      // allowed values.
      $count = $this->account->{$field_name}->count();
      if ($count >= $field->getCardinality()) {
        $message = t('You cannot register to this group, as you have reached your maximum allowed subscriptions.');
        $redirect = TRUE;
      }
    }

    if ($redirect) {
      drupal_set_message($message, 'warning');
      $url = $this->group->urlInfo();
      return $this->redirect($url['route_name'], $url['route_parameters']);
    }

    if (og_user_access($this->group, 'subscribe', $this->account) || og_user_access($this->group, 'subscribe without approval', $this->account)) {
      // Show the user a subscription confirmation.
      $context = array(
        'group' => $this->group,
        'field_name' => $field_name,
      );

      $form = \Drupal::formBuilder()->getForm('\Drupal\og_ui\Form\GroupJoin', $context);
      return $form;
    }

    throw new AccessDeniedHttpException();
  }

  public function unsubscribe($entity_type, $entity_id) {
      $this->loadGroup($entity_type, $entity_id);

    if (!$this->group || !og_is_group($this->group)) {
      // Not a valid entity, or not a group.
      throw new NotFoundHttpException();
    }

    // Check the user isn't the manager of the group.
    if ($this->group->uid != $this->account->id()) {
      if (og_is_member($this->group, 'user', $this->account, array(OG_STATE_ACTIVE, OG_STATE_PENDING))) {
        // Show the user a subscription confirmation.
        $context = array('group' => $this->group);
        $form = \Drupal::formBuilder()->getForm('\Drupal\og_ui\Form\GroupLeave', $context);
        return $form;
      }
      throw new AccessDeniedHttpException();
    }
    else {
      $label = $this->group->label();
      drupal_set_message(t('As the manager of %group, you can not leave the group.', array('%group' => $label)));
      $url = $this->group->urlInfo();
      return $this->redirect($url['route_name'], $url['route_parameters']);
    }
  }

  public function unsubscribeAccess($entity_type, $entity_id) {
    $this->loadGroup($entity_type, $entity_id);
  }

}
