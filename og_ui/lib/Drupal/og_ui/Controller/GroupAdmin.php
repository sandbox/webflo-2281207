<?php

/**
 * @file
 * Contains \Drupal\og_ui\Controller\GroupAdmin.
 */

namespace Drupal\og_ui\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class GroupAdmin extends OgControllerBase {

  public function addUser($entity_type, $entity_id) {
    $this->loadGroup($entity_type, $entity_id);
    if (!og_ui_user_access_group('add user', $entity_type, $entity_id)) {
      throw new AccessDeniedHttpException();
    }

    include_once drupal_get_path('module', 'og_ui') . '/og_ui.admin.inc';
    return drupal_get_form('og_ui_add_users', $entity_type, $entity_id);
  }

}
