<?php

/**
 * @file
 * Contains \Drupal\og_ui\Controller\OgControllerBase.
 */

namespace Drupal\og_ui\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class OgControllerBase extends ControllerBase {

  /**
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $group;

  /**
   * @var \Drupal\user\UserInterface
   */
  protected $account;

  function __construct() {
    $this->account = $this->entityManager()->getStorage('user')->load(\Drupal::currentUser()->id());
  }

  /**
   * Load group entity.
   *
   * @param $entity_type
   *
   * @param $entity_id
   *
   * @return \Drupal\Core\Entity\EntityInterface
   */
  protected function loadGroup($entity_type, $entity_id) {
    if (!$this->entityManager()->getDefinition($entity_type)) {
      throw new NotFoundHttpException();
    }

    $storage = $this->entityManager()->getStorage($entity_type);
    $this->group = $storage->load($entity_id);

    if (!$this->group) {
      throw new NotFoundHttpException();
    }
    return $this->group;
  }

}


