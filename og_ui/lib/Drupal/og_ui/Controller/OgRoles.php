<?php

/**
 * @file
 * Contains \Drupal\og_ui\Controller\OgRoles.
 */

namespace Drupal\og_ui\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class OgRoles extends ControllerBase {

  public function overview() {
    $header = array(t('Group type'), t('Operations'));
    $rows = array();

    foreach (og_get_all_group_bundle() as $entity_type => $bundles) {
      $entity_info = \Drupal::entityManager()->getDefinition($entity_type);
      foreach ($bundles as $bundle_name => $bundle_label) {
        $row = array();
        $row[] = array('data' => check_plain($entity_info->getLabel() . ' - ' . $bundle_label));
        $row[] = array('data' => l(t('edit'), "admin/config/group/roles/$entity_type/$bundle_name"));

        $rows[] = $row;
      }
    }

    $build['roles_table'] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => t('No group types available. Edit or <a href="@url">add a new content type</a> and set it to behave as a group.', array('@url' => url('admin/structure/types/add'))),
    );

    return $build;
  }

  public function content($entity_type, $bundle) {
    $entity_type = $this->entityManager()->getDefinition($entity_type);
    if (!$entity_type) {
      throw new NotFoundHttpException();
    }

    $bundles = $this->entityManager()->getBundleInfo($entity_type->id());
    if (!isset($bundles[$bundle])) {
      throw new NotFoundHttpException();
    }

    $bundle = $bundles[$bundle];
  }

}
