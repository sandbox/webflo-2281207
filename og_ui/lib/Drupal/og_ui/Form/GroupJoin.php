<?php

/**
 * @file
 * Contains \Drupal\og_ui\Form\GroupJoin.
 */

namespace Drupal\og_ui\Form;

use Drupal\Core\Form\ConfirmFormBase;

class GroupJoin extends ConfirmFormBase {

  /**
   * @var \Drupal\Core\Entity\ContentEntityInterface
   */
  protected $group;

  protected $field_name;

  public function buildForm(array $form, array &$form_state) {
    $context = reset($form_state['build_info']['args']);

    $this->group = $context['group'];
    $this->field_name = $context['field_name'];

    // Indicate the OG membership state (active or pending).
    $state = og_user_access($this->group, 'subscribe without approval') ? OG_STATE_ACTIVE : OG_STATE_PENDING;
    if (!$this->group->access('view') && $state == OG_STATE_ACTIVE) {
      $state = $this->config('og.settings')->get('deny_subscribe_without_approval', TRUE) ? OG_STATE_PENDING : OG_STATE_ACTIVE;
    }

    // Add group membership form.
    $group_type = $this->group->getEntityTypeId();
    $gid = $this->group->id();

    $og_membership = og_membership_create($group_type, $gid, 'user', $this->currentUser()->id(), $this->field_name, array('state' => $state));
    $form_state['og_membership'] = $og_membership;
    $form_state['form_display'] = entity_get_form_display($og_membership->getEntityTypeId(), $og_membership->bundle(), 'default');
    $form_state['form_display']->buildForm($og_membership, $form, $form_state);

    if ($state == OG_STATE_ACTIVE && !empty($form[OG_MEMBERSHIP_REQUEST_FIELD])) {
      // Hide the user request field.
      $form[OG_MEMBERSHIP_REQUEST_FIELD]['#access'] = FALSE;
    }

    $form['group_type'] = array('#type' => 'value', '#value' => $group_type);
    $form['gid'] = array('#type' => 'value', '#value' => $gid);
    $form['field_name'] = array('#type' => 'value', '#value' => $this->field_name);

    return parent::buildForm($form, $form_state);
  }

  /**
   * Returns the question to ask the user.
   *
   * @return string
   *   The form question. The page title will be set to this value.
   */
  public function getQuestion() {
    $title = $this->group->access('view') ? $this->group->label() : $this->t('Private group');
    return $this->t('Are you sure you want to join the group %title?', array('%title' => $title));
  }

  /**
   * Returns the route to go to if the user cancels the action.
   *
   * @return array
   *   An associative array with the following keys:
   *   - route_name: The name of the route.
   *   - route_parameters: (optional) An associative array of parameter names
   *     and values.
   *   - options: (optional) An associative array of additional options. See
   *     \Drupal\Core\Routing\UrlGeneratorInterface::generateFromRoute() for
   *     comprehensive documentation.
   */
  public function getCancelRoute() {
    // TODO: Implement getCancelRoute() method.
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'og_ui_confirm_subscribe';
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param array $form_state
   *   An associative array containing the current state of the form.
   */
  public function submitForm(array &$form, array &$form_state) {
    $og_membership = $form_state['og_membership'];
    $form_state['form_display']->extractFormValues($og_membership, $form, $form_state);
    $og_membership->save();

    $group_type = $form_state['values']['group_type'];
    $gid = $form_state['values']['gid'];
    $group = entity_load($group_type, $gid);

    if ($group->access('view')) {
      $form_state['redirect'] = $group->getSystemPath();
    }
    else {
      // User doesn't have access to the group entity, so redirect to front page,
      // with a message.
      $form_state['redirect'] = '<front>';
      drupal_set_message(t('Your subscription request was sent.'));
    }
  }

}
