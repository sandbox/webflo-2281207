<?php

/**
 * @file
 * Contains \Drupal\og_ui\Form\GroupLeave.
 */

namespace Drupal\og_ui\Form;

use Drupal\Core\Form\ConfirmFormBase;

class GroupLeave extends ConfirmFormBase {

  /**
   * @var \Drupal\Core\Entity\ContentEntityInterface
   */
  protected $group;

  protected $field_name;

  public function buildForm(array $form, array &$form_state) {
    $context = reset($form_state['build_info']['args']);
    $this->group = $context['group'];
    return parent::buildForm($form, $form_state);
  }

  /**
   * Returns the question to ask the user.
   *
   * @return string
   *   The form question. The page title will be set to this value.
   */
  public function getQuestion() {
    $title = $this->group->label();
    return $this->t('Are you sure you want to unsubscribe from the group %title?', array('%title' => $title));
  }

  /**
   * Returns the route to go to if the user cancels the action.
   *
   * @return array
   *   An associative array with the following keys:
   *   - route_name: The name of the route.
   *   - route_parameters: (optional) An associative array of parameter names
   *     and values.
   *   - options: (optional) An associative array of additional options. See
   *     \Drupal\Core\Routing\UrlGeneratorInterface::generateFromRoute() for
   *     comprehensive documentation.
   */
  public function getCancelRoute() {
    return $this->group->urlInfo();
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'og_ui_confirm_unsubscribe';
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param array $form_state
   *   An associative array containing the current state of the form.
   */
  public function submitForm(array &$form, array &$form_state) {
    og_ungroup($this->group->getEntityTypeId(), $this->group->id());
    $form_state['redirect'] = '<front>';

    if ($this->group->access('view')) {
      $form_state['redirect'] = $this->group->getSystemPath();
    }
  }

}
