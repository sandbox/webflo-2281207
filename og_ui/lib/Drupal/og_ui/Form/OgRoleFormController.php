<?php

/**
 * @file
 * Contains \Drupal\og_ui\Form\OgRoleFormController.
 */

namespace Drupal\og_ui\Form;

use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Form\FormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class OgRoleFormController extends FormBase {

  protected $entityType;

  protected $bundle;

  function __construct(Request $request, EntityManagerInterface $entity_manager) {
    $this->request = $request;
    $this->entityManager = $entity_manager;

    $entity_type = $request->get('entity_type');
    $bundle = $request->get('bundle');

    $this->entityType = $this->entityManager->getDefinition($entity_type);
    if (!$entity_type) {
      throw new NotFoundHttpException();
    }

    $bundles = $this->entityManager->getBundleInfo($this->entityType->id());
    if (!isset($bundles[$bundle])) {
      throw new NotFoundHttpException();
    }

    $this->bundle = $bundle;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request'),
      $container->get('entity.manager')
    );
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'og_ui_admin_role';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param array $form_state
   *   An associative array containing the current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, array &$form_state) {
    $header = array(
      t('Name'),
      t('Operations'),
    );

    $query = \Drupal::entityQuery('og_user_role')
      ->condition('group_type', $this->entityType->id())
      ->condition('group_bundle', $this->bundle);

    $rids = $query->execute();
    $roles = $this->entityManager->getStorage('og_user_role')->loadMultiple($rids);

    $rows = array();

    foreach ($roles as $role) {
      $operations = array();

      $row = array(
        'name' => $role->name,
        'operations' => array(
          'data' => array(
            '#type' => 'operations',
            '#links' => array(),
          ),
        ),
      );

      $operations['edit'] = array(
        'title' => $this->t('edit'),
        'route_name' => 'og_ui.role_permission',
        'route_parameters' => array(
          'og_user_role' => $role->id(),
        ),
        'weight' => 0,
      );

      $row['operations']['data']['#links'] = $operations;
      $rows[] = $row;
    }

    /*
    if ($gid) {
      $group = entity_load($group_type, $gid);
      $default_access = $group && og_is_group_default_access($group_type, $group);
    }
    else {
      $default_access = FALSE;
    }

    foreach ($role_names as $rid => $name) {
      $text = !$default_access ? t('edit permissions') : t('view permissions');
      $path = $gid ? "group/$group_type/$gid/admin" : 'admin/config/group';
      $permissions = l($text, "$path/permission/$rid/edit");

      if (!$default_access && !in_array($name, array(OG_ANONYMOUS_ROLE, OG_AUTHENTICATED_ROLE))) {
        $rows[] = array(check_plain($name), l(t('edit role'), "$path/role/$rid/edit"), $permissions);
      }
      else {
        $rows[] = array(check_plain($name), t('locked'), $permissions);
      }
    }

    $rows[] = array(array('data' => drupal_render($form['name']) . drupal_render($form['add']), 'colspan' => 4, 'class' => 'edit-name'));

    hide($form['actions']);
    $output = drupal_render_children($form);
    $output .= _theme('table', array('header' => $header, 'rows' => $rows));
    $output .= render($form['actions']);
    */

    $form['roles'] = array(
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    );

    $form['name'] = array(
      '#type' => 'textfield',
      '#label' => 'Role',
    );

    $form['actions'] = array(
      '#type' => 'actions'
    );

    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Create'),
    );

    return $form;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param array $form_state
   *   An associative array containing the current state of the form.
   */
  public function submitForm(array &$form, array &$form_state) {
    $name = $form_state['values']['name'];
    $role = $this->entityManager->getStorage('og_user_role')->create(array(
      'group_type' => $this->entityType->id(),
      'group_bundle' => $this->bundle,
      'name' => $name,
      'label' => $name,
      'id' => implode('__', array($this->entityType->id(), $this->bundle, $name))
    ));
    $role->save();
  }


}
