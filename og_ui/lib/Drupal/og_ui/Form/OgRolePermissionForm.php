<?php

/**
 * @file
 * Contains \Drupal\og_ui\Form\OgRolePermissionForm.
 */

namespace Drupal\og_ui\Form;

use Drupal\og\Entity\OgRole;

class OgRolePermissionForm extends OgRolePermissionFormBase {

  /**
   * The OG user role entity.
   *
   * @var OgRole
   */
  protected $roles;

  /**
   * {@inheritdoc}
   */
  protected function getRoles() {
    return $this->roles;
  }

  public function buildForm(array $form, array &$form_state, $entity_type = NULL, $bundle = NULL) {
    $query = \Drupal::entityQuery('og_user_role')
      ->condition('group_type', $entity_type)
      ->condition('group_bundle', $bundle);

    $rids = $query->execute();
    $this->roles = \Drupal::entityManager()->getStorage('og_user_role')->loadMultiple($rids);

    return $this->buildPermissionForm($form, $form_state);
  }

}
