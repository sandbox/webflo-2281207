<?php

/**
 * @file
 * Contains \Drupal\og_ui\Form\OgRolePermissionSpecificForm.
 */

namespace Drupal\og_ui\Form;

use Drupal\og\Entity\OgRole;

class OgRolePermissionSpecificForm extends OgRolePermissionForm {

  /**
   * The OG user role entity.
   *
   * @var OgRole
   */
  protected $roles;

  /**
   * {@inheritdoc}
   */
  protected function getRoles() {
    return array($this->roles->id() => $this->roles);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, array &$form_state, OgRole $og_user_role = NULL) {
    $this->roles = $og_user_role;
    return parent::buildPermissionForm($form, $form_state);
  }
}
